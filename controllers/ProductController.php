<?php

namespace app\controllers;

use app\models\Cart;
use app\models\Product;
use app\models\ThermosTypes;
use yii\app;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use yii;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGet_all()
    {
        $types = ThermosTypes::findAll(['enabled' => 1]);
        $data = array();
        foreach ($types as $type) {
            $items = Product::find()->select("id,name,image,price_sell,description,stock")
                // ->where("stock > 0")
                ->Where("enabled=1")
                ->andWhere("type=" . $type->id)
                ->all();

            $data[$type->name] = $items;
        }
        if ($data != null) {
            $response = [
                "data" => $data,
                "status" => "success",
            ];
        } else {
            $response = [
                "data" => null,
                "status" => "error",
            ];
        }

        return $response;
    }

    public function actionGet_find_by_type()
    {
        $request = Yii::$app->request;
        $type = $request->post("type");
        $items = Product::find()->select("id,name,image,price_sell")
            ->where("stock > 0")
            ->andWhere("enabled=1")
            ->andWhere("Type=" . $type)
            ->all();

        if ($items != null) {

            $response = [
                "data" => $items,
                "status" => "success",
            ];
        } else {
            $response = [
                "data" => null,
                "status" => "error",
            ];
        }
        return $response;
    }


    public function actionTest()
    {
        return "works";
    }

    public function actionAdd_items()
    {
        //return "hehue";
        $request = Yii::$app->request;
        $data = $request->post('data');
        $id = $data['id'];
        $cant = $data['cant'];

        $item = Cart::findOne($id);
        $item->cant = $cant;
        $item->price = $item->price * $item->cant;
        //  return $item;
        $response = [
            "status" => 'error',
            "message" => "Item Can'tUpdated"
        ];
        if ($item->update(false)) {
            $response = [
                "status" => 'success',
                "message" => 'Item Updated'
            ];
        }

        return $response;
    }
}
