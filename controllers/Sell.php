<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sell".
 *
 * @property int $id
 * @property double $sub_total
 * @property double $total
 * @property double $shippin 
 * @property string $date
 * @property string $order_number
 * @property string $temp_token
 * @property int $id_user
 * @property int $habilitado
 *
 * @property DetailsSell[] $detailsSells
 * @property UsersCustom $user
 */
class Sell extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sell';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_total', 'total', 'shippin'], 'number'],
            [['date'], 'safe'],
            [['order_number', 'temp_token'], 'string'],
            [['id_user', 'habilitado'], 'integer'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => UsersCustom::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_total' => 'Sub Total',
            'total' => 'Total',
            'shippin' => 'Shippin',
            'date' => 'Date',
            'id_user' => 'Id User',
            'order_number' => 'Order Number',
            'temp_token' => 'Temp Token',
            'habilitado' => 'Habilitado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailsSells()
    {
        return $this->hasMany(DetailsSell::className(), ['id_sell' => 'id'])->select("id,id_sell,id_product")->with("productDto");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UsersCustom::className(), ['id' => 'id_user']);
    }
}
