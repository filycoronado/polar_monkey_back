<?php

namespace app\controllers;

use app\models\Cart;
use app\models\User;
use app\models\DetailsSell;
use app\models\Product;
use app\models\Cupon;
use app\models\Sell;
use app\models\UsersCustom;
use app\models\Visitas;
use app\models\Admins;
use yii;
use yii\app;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionLogin()
    {

        $modelClient = new UsersCustom();
        $request = Yii::$app->request;
        $modelClient->load(Yii::$app->getRequest()->getBodyParams(), 'user');
        $pass = md5($modelClient->password);
        $item = UsersCustom::find()->where("password='" . $pass . "'")->andWhere("email='" . $modelClient->email . "'")->one();

        if ($item != null) {
            $token = $this->Get_access_token();
            $item->access_token = $token;
            if ($item->update(false)) {
                $response = [
                    "data" => $item,
                    "status" => "success",
                ];
            } else {
                $response = [
                    "data" => "can´t update access token",
                    "status" => "error",
                ];
            }
        } else {
            $response = [
                "data" => "Credentials worng",
                "status" => "error",
            ];
        }
        return $response;
    }


    public function actionLogin_admin()
    {

        $modelClient = new UsersCustom();
        $request = Yii::$app->request;
        $modelClient->load(Yii::$app->getRequest()->getBodyParams(), 'user');
        $pass = md5($modelClient->password);

        $item = Admins::find()
            ->where("password='" . $pass . "'")
            ->andWhere("username='" . $modelClient->email . "'")
            ->one();

        if ($item != null) {
            $response = [
                "data" => $item,
                "status" => "success",
            ];
        } else {
            $response = [
                "data" => "Credentials worng",
                "status" => "error",
            ];
        }
        return $response;
    }

    public function actionRegister()
    {
        $modelClient = new UsersCustom();
        $modelClient->load(Yii::$app->getRequest()->getBodyParams(), 'user');
        $modelClient->password = md5($modelClient->password);
        $modelClient->create_date = date("Y-m-d");

        if (!($this->Validate_email($modelClient->email) == 1)) {
            if ($modelClient->save(false)) {
                $response = [
                    "data" => $modelClient,
                    "status" => "success",
                ];
                $modelClient->enabled = 0;
                if ($modelClient->update(false)) {
                    $this->actionMail_confirm($modelClient->id);
                }
            } else {
                $response = [
                    "data" => "Error can't save user",
                    "status" => "error",
                ];
            }
        } else {
            $response = [
                "data" => "Email al ready exist.",
                "status" => "error",
            ];
        }

        return $response;
    }
    public function actionConfirm_mail($id)
    {
        $item = UsersCustom::findOne($id);
        $item->enabled = 1;
        if ($item->update(false)) {
            $response = [
                "data" => "true",
                "status" => "success",
            ];
        } else {
            $response = [
                "data" => "false",
                "status" => "error",
            ];
        }
        return  $response;
    }


    public function actionMail_confirm($id)
    {


        $url = "http://polarmonkeythermos.com/#/confirm/" . $id; //urle test
        $user = UsersCustom::findOne($id);
        $var = Yii::$app->mailer->compose('layouts/html', ['content' => "<a href='$url'>Confirm Account</a>"])
            ->setFrom('no-reply@polarmonkey.com')
            ->setTo($user->email)
            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->setTextBody('Body Text')
            ->send();
        return $var;
    }

    public function actionMail_recovery()
    {
        $modelClient = new UsersCustom();
        $modelClient->load(Yii::$app->getRequest()->getBodyParams(), 'user');
        $item = UsersCustom::find()->where("email='" . $modelClient->email . "'")->one();
        $token = $this->Get_access_token();
        $url = "http://polarmonkeythermos.com/password-recovery?token=" . $token . "&id=" . $item->id;
        $var = Yii::$app->mailer->compose('layouts/html', ['content' => "<a href='$url'>Click To Change Password</a>"])
            ->setFrom('no-reply@polarmonkey.com')
            ->setTo($item->email)
            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->setTextBody('Body Text')
            ->send();
        return $var;
    }
    public function Get_access_token()
    {
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(16);

        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        return $token;
    }

    public function Validate_email($email)
    {
        $item = UsersCustom::find()->where("email='" . $email . "'")->andWhere("enabled=1")->one();
        if ($item != null) {
            return 1; //error
        } else {
            return 0; // continue
        }
    }

    public function actionSave_cart()
    {
        $Cart = new Cart();
        $request = Yii::$app->request;
        $modelClient = $request->post("cart");


        $image_example = $modelClient['image_example'];
        $image_example2 = $modelClient['image_example2'];
        $image_icon = $modelClient['image_icon'];
        $image_icon2 = $modelClient['image_icon2'];
        if ($image_example != null) {
            $image_example = $this->base64ToImage($modelClient['id_user'], "Front_Example_", $image_example);
        }
        if ($image_example2 != null) {
            $image_example2 = $this->base64ToImage($modelClient['id_user'], "Back_Example_", $image_example2);
        }


        if ($image_icon != "") {
            $image_icon = $this->base64ToImage($modelClient['id_user'], "FrontIcon_", $image_icon);
        }
        if ($image_icon2 != "") {
            $image_icon2 = $this->base64ToImage($modelClient['id_user'], "BackIcon_", $image_icon2);
        }


        $Cart->image_icon = $image_icon;
        $Cart->image_icon2 = $image_icon2;
        $Cart->image_example = $image_example;
        $Cart->image_example2 = $image_example2;
        $Cart->date = date("y-m-d");
        $Cart->status = 1;
        $Cart->habilitado = 1;
        $Cart->cant = 1;
        $Cart->id_user = $modelClient['id_user'];
        $Cart->id_product = $modelClient['id_product'];
        $product = Product::findOne($modelClient['id_product']);
        $Cart->price = $product->price_sell;
        $Cart->font = $modelClient['font'];
        $Cart->message = $modelClient['message'];
        $Cart->meesage2 = $modelClient['message2'];

        if ($Cart->save(false)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actionGet_cart($id)
    {

        return Cart::find()
            ->where("id_user=" . $id)
            ->andWhere("habilitado=1")
            ->andWhere("status=1")->with("product")->asArray()
            ->all();
    }
    public function actionGet_cart2($id)
    {

        return Cart::find()->select("Count(*) as total")
            ->where("id_user=" . $id)
            ->andWhere("habilitado=1")
            ->andWhere("status=1")->with("product")->asArray()
            ->all();
    }


    public function actionGet_cart3($token)
    {



        return Cart::find()->select("Count(*) as total")
            ->where("temp_token='" . $token . "'")
            ->andWhere("habilitado=1")
            ->andWhere("status=1")
            ->all();
    }
    public function actionDelete_car_item($id)
    {
        $item = Cart::findOne($id);
        $item->habilitado = 0;

        if ($item->update(false)) {
            $response = [
                "status" => "success",
            ];
        } else {
            $response = [
                "status" => "error",
            ];
        }
        return $response;
    }

    public function actionGet_product($id)
    {
        return Product::find()
            ->where("id=" . $id)
            ->with("type0")
            ->asArray()
            ->one();;
    }
    public function actionCheck_cupon($cupon, $iduser, $tmptoken)
    {
        $today = date("Y-m-d");
        $cupon = Cupon::find()
            ->where("name='" . $cupon . "'")->andWhere("enabled=1")->one();
        $user = UsersCustom::find()->where("access_token= '" . $tmptoken . "'")->one();
        $selluser = Sell::find()->where("id_user=" . $user->id)->one();
        $selltemp = Sell::find()->where("temp_token='" . $tmptoken . "'")->one();
        if ($selluser != null || $selltemp != null) {
            return    $response = [
                "status" => "error",
                "msj" => "Coupon Used"
            ];
        } else {
            if ($cupon->date >= $today) {
                return    $response = [
                    "status" => "success",
                    "value" => $cupon->percentaje,
                    "msj" => "Valid Coupon"
                ];
            } else {
            }
        }
    }



    public function actionGet_all_cupons()
    {
        return Cupon::find()->where("enabled=1")->all();
    }

    function base64ToImage($id, $name, $image)
    {

        $folderPath = "./../files/";
        $image_parts = explode(";base64,", $image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $today = date("Y-m-d");
        $time = strtotime(date("H:i:s"));


        $filename = $name . $id . "_" . $today . "_" . $time . '.' . $image_type;
        $file = $folderPath . $name . $id . "_" . $today . "_" . $time . '.' . $image_type;
        if (file_put_contents($file, $image_base64)) {
            return $filename;
        }
        return false;
    }


    public function actionUpdate_password()
    {
        $modelClient = new UsersCustom();
        $modelClient->load(Yii::$app->getRequest()->getBodyParams(), 'user');
        $request = Yii::$app->request;
        $id = $request->post('id');
        $item = UsersCustom::findOne($id);
        $item->password = md5($modelClient->password);
        if ($item->update(false)) {
            $response = [
                "status" => "success",
            ];
        } else {
            $response = [
                "status" => "error",
            ];
        }
        return $response;
    }

    public function actionCheckout()
    {
        // Setup order information array with all items
        $params = [
            'method' => 'paypal',
            'intent' => 'sale',
            'order' => [
                'description' => 'Payment description',
                'subtotal' => 0.5,
                'shippingCost' => 0,
                'total' => 0.5,
                'currency' => 'USD',
                'items' => [
                    [
                        'name' => 'Item one',
                        'price' => 0.5,
                        'quantity' => 1,
                        'currency' => 'USD'
                    ]

                ]

            ]
        ];

        //  En esta acción, redirigirá al sitio web de PayPal para iniciar sesión con su cuenta de comprador y completar el pago
        Yii::$app->PayPalRestApi->checkOut($params);
    }

    public function actionMakepayment()
    {

        // Setup order information array 
        $params = [
            'order' => [
                'description' => 'Payment description',
                'subtotal' => 44,
                'shippingCost' => 0,
                'total' => 44,
                'currency' => 'USD',
            ]
        ];
        // In case of payment success this will return the payment object that contains all information about the order
        // In case of failure it will return Null
        return  Yii::$app->PayPalRestApi->processPayment($params);
    }


    public function actionSave_sell()
    {
        $request = Yii::$app->request;
        $array = $request->post('products');
        $token = $request->post('token');
        $cupon = $request->post('cupon');
        $sell = new Sell();
        $sell->date = date("Y-m-d");
        $sell->habilitado = 1;
        $sell->shippin = 0;
        $sell->sub_total = 0;
        $sell->total = 0;
        $sell->date = date("Y-m-d");
        $sell->order_number = $this->random_strings(8);
        $id = $request->post('id_user');
        if ($token != null) {

            Cart::updateAll(['id_user' => $id], "temp_token = '" . $token . "'");
        }
        $sell->id_user = $id;
        if ($sell->save(false)) {
            $total = 0;
            $id_user = 0;
            foreach ($array as $key => $value) {
                $details = new DetailsSell();
                $details->date = date("Y-m-d");
                $details->id_sell = $sell->id;
                $details->id_product = $value['id_product'];
                $details->price = $value['price'];
                $details->habilitado = 1;
                $id_user = $value['id_user'];
                if ($details->save(false)) {
                    $total += $value['price'];
                    $this->actionAttachmail($value['id'], $sell->id);
                    $this->Deleteitem($value['id']);
                } else {
                    $response = [
                        "status" => "error"
                    ];
                    $sell->habilitado = 0;
                    $sell->update(false);
                }
            }
            $sell->total = $total;
            //$sell->id_user = $id_user;
            $sell->sub_total = $sell->total;


            if ($total >= 59) {
                $sell->shippin = 0;
            } else {
                $sell->shippin = 7.49;
                $sell->total = $sell->total + $sell->shippin;
            }
            if ($cupon != "") {
                $localtotal = $sell->total;
                $cup = Cupon::find()->where("name ='" . $cupon . "'")->one();

                $sell->total = $localtotal - (($localtotal *  $cup->percentaje) / 100);
                $sell->cupon = $cupon;
            }
            if ($sell->update(false)) {


                $response = [
                    "status" => "success",
                ];
            } else {
                $response = [
                    "status" => "error",
                ];
            }
        } else {
            $response = [
                "status" => "error",
            ];
        }

        return  $response;
    }

    public function actionGet_user_by_token($token)
    {
        return UsersCustom::find()->where("access_token='" . $token . "'")->one();
    }
    public function actionSave_sell_no_login()
    {
        $request = Yii::$app->request;
        $array = $request->post('products');
        $sell = new Sell();
        $sell->date = date("Y-m-d");
        $sell->habilitado = 1;
        $sell->shippin = 0;
        $sell->sub_total = 0;
        $sell->total = 0;
        $sell->date = date("Y-m-d");
        $sell->order_number = $this->random_strings(8);
        $sell->temp_token = $request->post('token');
        if ($sell->save(false)) {
            $total = 0;
            $id_user = 0;
            foreach ($array as $key => $value) {
                $details = new DetailsSell();
                $details->date = date("Y-m-d");
                $details->id_sell = $sell->id;
                $details->id_product = $value['id_product'];
                $details->price = $value['price'];
                $details->habilitado = 1;
                //  $id_user = $value['id_user'];
                if ($details->save(false)) {
                    $total += $value['price'];
                    $this->actionAttachmail($value['id'], $sell->id);
                    $this->Deleteitem($value['id']);
                } else {
                    $response = [
                        "status" => "error"
                    ];
                    $sell->habilitado = 0;
                    $sell->update(false);
                }
            }
            $sell->total = $total;
            //  $sell->id_user = $id_user;
            $sell->sub_total = $sell->total;
            if ($total >= 59) {
                $sell->shippin = 0;
            } else {
                $sell->shippin = 7.49;
                $sell->total = $sell->total + $sell->shippin;
            }
            if ($sell->update(false)) {
                $response = [
                    "status" => "success",
                ];
            } else {
                $response = [
                    "status" => "error",
                ];
            }
        } else {
            $response = [
                "status" => "error",
            ];
        }

        return  $response;
    }

    public function actionAttachmail($id_cart, $sell_id)
    {

        $sell = Sell::findOne($sell_id);
        $item = Cart::findOne($id_cart);
        $customer = UsersCustom::findOne($item->id_user);
        $image_logo = "";
        $imag = null;
        $imag2 = null;
        if ($item->image_icon != null) {
            $image_logo = Yii::getAlias('@app/files/' . $item->image_icon);
        }
        if ($item->image_example) {
            $imag = Yii::getAlias('@app/files/' . $item->image_example);
        }

        if ($item->image_example2) {
            $imag2 = Yii::getAlias('@app/files/' . $item->image_example2);
        }


        $conten = "Customer: " . $customer->email . " Address: " . $customer->address . "/ Font:" . $item->font . " Font Zise:" . $item->font_size . " Type:" . $item->font_type . "Text:" . $item->message . "text 2" . $item->meesage2;
        $conten .= "Order:" . $sell->order_number;
        $var = Yii::$app->mailer->compose('layouts/html', ['content' => $conten])
            ->setFrom('no-reply@polarmonkey.com')
            ->setTo("ruben@polarmonkeythermos.com") //ruben@polarmonkeythermos.com





            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->setTextBody('Body Text');
        if ($imag != null) {
            $var->attach($imag);
        }
        if ($imag2 != null) {
            $var->attach($imag2);
        }
        if ($image_logo != null) {
            $var->attach($image_logo);
        }
        return $var->send();
        //return $var;
    }
    public function actionGet_user_by_id($id)
    {

        $item = UsersCustom::findOne($id);
        if ($item != null) {
            return $item;
        } else {
            return null;
        }
    }

    public function actionGenerate_user()
    {
        $request = Yii::$app->request;
        $data = $request->post('user');
        $token = $request->post('token');
        $client = new UsersCustom();
        $client->attributes = $data;
        $client->password = md5("123456");
        $client->access_token = $token;
        $client->enabled = 0;

        if ($client->save(false)) {
            Cart::updateAll(['id_user' => $client->id], "temp_token = '" . $token . "'");
            return $client;
        } else {
            return null;
        }
    }

    public function actionGet_user_orders_by_id($id)
    {
        $orders =   Sell::find()
            ->where("id_user=" . $id)
            ->with("detailsSells")
            ->asArray()
            ->all();
        $response = [
            "status" => "error",
            "message" => "items not found"
        ];
        if ($orders != null) {
            $response = [
                "status" => "success",
                "message" => "Orders Found",
                "data" => $orders
            ];
        }

        return $response;
    }

    public function actionUpdate_user()
    {
        $request = Yii::$app->request;
        $modelClient = $request->post('user');
        $id = $modelClient['id'];
        $item = UsersCustom::findOne($id);
        $item->address = $modelClient['address'];
        $item->zipcode = $modelClient['zipcode'];
        $item->city = $modelClient['city'];
        $item->state = $modelClient['state'];
        if ($item->update()) {
            return $item;
        } else {
            return null;
        }
    }

    public function Deleteitem($id)
    {
        $item = Cart::findOne($id);
        $item->habilitado = 0;

        if ($item->update(false)) {
            $response = [
                "status" => "success",
            ];
        } else {
            $response = [
                "status" => "error",
            ];
        }
        return $response;
    }

    public function actionSend_quote()
    {
        $request = Yii::$app->request;

        $email = $request->post('email');
        $back = $request->post('back');
        $front = $request->post('front');
        $color = $request->post('color');


        $folderPath = "./../files/";
        $imageExample = $front['image_example'];
        $imageIcon = $front['image_icon'];
        $Fmessage = $front['message'];
        $Ffont = $front['font'];
        $Ffont_size = $front['font_size'];
        if ($imageIcon != null) {
            $name = "QuoteImageIconFrontSide";
            $image_parts = explode(";base64,", $imageIcon);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $today = date("Y-m-d");
            $time = strtotime(date("H:i:s"));
            $filename1 = $name  . "_" . $today . "_" . $time . '.' . $image_type;
            $file = $folderPath . $filename1;
            if (file_put_contents($file, $image_base64)) {
                $imageIcon = Yii::getAlias('@app/files/' . $filename1);
            }
        }
        if ($imageExample != null) {
            $name = "ExampleImageIconFrontSide";
            $image_parts = explode(";base64,", $imageExample);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $today = date("Y-m-d");
            $time = strtotime(date("H:i:s"));
            $filename2 = $name  . "_" . $today . "_" . $time . '.' . $image_type;

            $file = $folderPath . $filename2;
            if (file_put_contents($file, $image_base64)) {
                $imageExample = Yii::getAlias('@app/files/' . $filename2);
            }
        }

        $imageExample2 = $back['image_example2'];
        $imageIcon2 = $back['image_icon2'];
        $Bmessage = $back['message'];
        $Bfont = $back['font'];
        $Bfont_size = $back['font_size'];

        if ($imageIcon2 != null) {
            $name = "QuoteImageIconBackSide";
            $image_parts = explode(";base64,", $imageIcon2);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $today = date("Y-m-d");
            $time = strtotime(date("H:i:s"));
            $filename1 = $name  . "_" . $today . "_" . $time . '.' . $image_type;
            $file = $folderPath . $filename1;
            if (file_put_contents($file, $image_base64)) {
                $imageIcon2 = Yii::getAlias('@app/files/' . $filename1);
            }
        }
        if ($imageExample2 != null) {
            $name = "ExampleImageIconBackSide";
            $image_parts = explode(";base64,", $imageExample2);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $today = date("Y-m-d");
            $time = strtotime(date("H:i:s"));
            $filename2 = $name  . "_" . $today . "_" . $time . '.' . $image_type;

            $file = $folderPath . $filename2;
            if (file_put_contents($file, $image_base64)) {
                $imageExample2 = Yii::getAlias('@app/files/' . $filename2);
            }
        }



        $conten =
            "Color Selected:" . $color .
            "Customer: " . $email .
            "Side Front:  Font:" . $Ffont .
            " Font Zise:" . $Ffont_size .
            "Text:" . $Fmessage .
            "Back Side: Font:" . $Bfont .
            " Font Zise:" . $Bfont_size .
            "Text:" . $Bmessage;
        $var = Yii::$app->mailer->compose('layouts/html', ['content' => $conten])
            ->setFrom($email)
            ->setTo("rubennoriega082@gmail.com")
            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->setTextBody('Body Text');
        if ($imageIcon != null) {
            $var->attach($imageIcon);
        }
        if ($imageExample != null) {
            $var->attach($imageExample);
        }

        if ($imageIcon2 != null) {
            $var->attach($imageIcon2);
        }
        if ($imageExample2 != null) {
            $var->attach($imageExample2);
        }
        return $var->send();
    }

    public function actionSend_message()
    {
        //rubennoriega082@gmail.com
        $request = Yii::$app->request;
        $form = $request->post("form");
        $message = "" . $form['name'] . " " . $form["last_name"] . "Phone:" . $form["phone"] . "email:" . $form["email"] . "message:" . $form["message"];
        $var = Yii::$app->mailer->compose('layouts/html', ['content' => $message])
            ->setFrom('no-reply@polarmonkey.com')
            ->setTo("rubennoriega082@gmail.com")
            ->setSubject('Email enviado desde Yii2-Swiftmailer')
            ->setTextBody('Body Text')
            ->send();
        return $var;
    }


    public function actionAdd_tapa()
    {
        $request = Yii::$app->request;
        $idProduct = $request->post("idProduct");
        $id_user = $request->post("id_user");

        $item_product = Product::findOne($idProduct);
        $cart = new Cart();
        $cart->id_product = $idProduct;
        $cart->price = $item_product->price_sell;
        $cart->image_example = $item_product->image;
        $cart->image_example2 = $item_product->image;
        $cart->id_user = $id_user;
        $cart->habilitado = 1;
        $cart->cant = 1;
        $cart->status = 1;
        if ($cart->save(false)) {
            return 1;
        } else {
            return 0;
        }
    }


    public function actionAdd_tapa2()
    {
        $request = Yii::$app->request;
        $idProduct = $request->post("idProduct");
        $token = $request->post("token");

        $item_product = Product::findOne($idProduct);
        $cart = new Cart();
        $cart->id_product = $idProduct;
        $cart->price = $item_product->price_sell;
        $cart->image_example = $item_product->image;
        $cart->image_example2 = $item_product->image;
        $cart->id_user = null;
        $cart->habilitado = 1;
        $cart->cant = 1;
        $cart->status = 1;
        $cart->temp_token = $token;
        if ($cart->save(false)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actionGet_token()
    {
        return  uniqid();
    }


    public function actionSave_cupon()
    {
        $Cupon = new Cupon();
        $request = Yii::$app->request;
        $Cupon->setAttributes($request->post("cupon"));
        $old = Cupon::find()->where("name='" . $Cupon->name . "'")->one();

        $day = substr($Cupon->date, 0, 2);
        $month = substr($Cupon->date, 2, 2);
        $year = substr($Cupon->date, 4, 4);
        $stringdate = $day . "-" . $month . "-" . $year;
        $Cupon->date = date("Y-m-d", strtotime($stringdate));
        if ($old != null) {
            $old = Cupon::findOne($old->id);
            $old->date = date("Y-m-d", strtotime($stringdate));
            $old->enabled = $Cupon->enabled;
            $old->percentaje = $Cupon->percentaje;

            if ($old->update(false)) {
                return  $response = [
                    "status" => "success",
                    "data" => "",
                    "message" => "Item Updated Successfully"
                ];
            } else {
                return  $response = [
                    "status" => "error",
                    "data" => "",
                    "message" => "Cant Update Cupon"
                ];
            }
        } else {
            //save
            if ($Cupon->save(false)) {
                return  $response = [
                    "status" => "success",
                    "data" => "",
                    "message" => "Item Saved Successfully"
                ];
            } else {
                return  $response = [
                    "status" => "error",
                    "data" => "",
                    "message" => "Cant save Cupon"
                ];
            }
        }
    }
    public function actionSave_cart_temp()
    {
        $Cart = new Cart();
        $request = Yii::$app->request;
        $modelClient = $request->post("cart");


        $image_example = $modelClient['image_example'];
        $image_example2 = $modelClient['image_example2'];
        $image_icon = $modelClient['image_icon'];
        $image_icon2 = $modelClient['image_icon2'];
        $acctoken =  $modelClient['access_token'];
        if ($image_example != null) {
            $image_example = $this->base64ToImage($acctoken, "Front_Example_", $image_example);
        }
        if ($image_example2 != null) {
            $image_example2 = $this->base64ToImage($acctoken, "Back_Example_", $image_example2);
        }

        if ($image_icon != "") {
            $image_icon = $this->base64ToImage($acctoken, "FrontIcon_", $image_icon);
        }
        if ($image_icon2 != "") {
            $image_icon2 = $this->base64ToImage($acctoken, "BackIcon_", $image_icon2);
        }


        $Cart->image_icon = $image_icon;
        $Cart->image_icon2 = $image_icon2;
        $Cart->image_example = $image_example;
        $Cart->image_example2 = $image_example2;
        $Cart->date = date("y-m-d");
        $Cart->status = 1;
        $Cart->habilitado = 1;
        $Cart->cant = 1;
        $Cart->id_product = $modelClient['id_product'];
        $product = Product::findOne($modelClient['id_product']);
        $Cart->price = $product->price_sell;
        $Cart->font = $modelClient['font'];
        $Cart->message = $modelClient['message'];
        $Cart->meesage2 = $modelClient['message2'];
        $Cart->temp_token = $acctoken;
        $Cart->id_user = null;
        if ($Cart->save(false)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actionBind_cart()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        /**$items = Cart::find()
            ->where("temp_token='" . $data['access_token'] . "'")
            ->all();**/


        Cart::updateAll(['id_user' => $data['id']], "temp_token = '" . $data['access_token'] . "'");
    }


    public function actionGet_cart_token($token)
    {

        return Cart::find()
            ->where("temp_token='" . $token . "'")
            ->andWhere("habilitado=1")
            ->andWhere("status=1")
            ->with("product")
            ->asArray()
            ->all();
    }

    function random_strings($length_of_string)
    {
        // String of all alphanumeric character 
        $str_result = '0123456789';
        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function actionSave_visit()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $visitas = new Visitas();
        $visitas->attributes = $data;
        $visitas->create_date = date("Y-m-d");
        $visitas->save(false);
    }

    function actionGet_products()
    {
        return Product::find()->where("enabled=1")->all();
    }

    function actionGet_visits()
    {
        $array = Visitas::find()->where("enabled=1")->all();
        return count($array);
    }

    function actionUpdate_product($id)
    {
        $item = Product::findOne($id);
        if ($item->stock == 0) {
            $item->stock = 1000;
        } else {
            $item->stock = 0;
        }
        $response = [
            "status" => "error",
            "message" => "Cant Update Item"

        ];
        if ($item->update(false)) {
            $response = [
                "status" => "success",
                "data" => $item,
                "message" => "Item Updated Successfully"
            ];
        }
        return $response;
    }
}
