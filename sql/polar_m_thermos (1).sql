-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2019 a las 00:54:00
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `polar_m_thermos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `habilitado` int(11) DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `font` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `font_size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `font_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_icon` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_example` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cant` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cart`
--

INSERT INTO `cart` (`id`, `id_product`, `id_user`, `status`, `date`, `habilitado`, `message`, `font`, `font_size`, `font_type`, `image_icon`, `image_example`, `cant`, `price`) VALUES
(15, 2, 2, 1, '2019-11-01', 0, 'Your Name', 'Pacifico', '20px', 'regular', '', 'Example_1_2019-11-01_1572646247.png', 1, 0.5),
(16, 2, 2, 1, '2019-11-01', 0, 'Your Name', 'Pacifico', '20px', 'regular', '', 'Example_1_2019-11-01_1572646247.png', 1, 0.5),
(17, 2, 2, 1, '2019-11-05', 0, 'Your Name', 'Pacifico', '20px', 'regular', '', 'Example_1_2019-11-05_1572912521.png', 1, 20.55),
(18, 6, 2, 1, '2019-11-05', 0, 'Your Name', 'Pacifico', '20px', 'regular', 'Icon_1_2019-11-05_1572913439.png', 'Example_1_2019-11-05_1572913439.png', 1, 20.55),
(20, 13, 7, 1, '2019-11-06', 1, 'Your Name', 'Pacifico', '20px', 'regular', 'Icon_7_2019-11-06_1573080325.jpeg', 'Example_7_2019-11-06_1573080325.png', 1, 16.99),
(21, 10, 7, 1, '2019-11-11', 1, 'Your Name', 'Pacifico', '20px', 'regular', 'Icon_7_2019-11-11_1573499562.png', 'Example_7_2019-11-11_1573499562.png', 1, 20.99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `details_sell`
--

CREATE TABLE `details_sell` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_sell` int(11) NOT NULL,
  `date` date NOT NULL,
  `price` double NOT NULL,
  `habilitado` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `details_sell`
--

INSERT INTO `details_sell` (`id`, `id_product`, `id_sell`, `date`, `price`, `habilitado`) VALUES
(7, 2, 6, '2019-11-05', 0.5, 1),
(8, 6, 6, '2019-11-05', 20.55, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1,
  `price_sell` double DEFAULT NULL,
  `price_buy` double DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `color` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capacity` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `name`, `type`, `image`, `create_date`, `enabled`, `price_sell`, `price_buy`, `stock`, `color`, `weight`, `description`, `capacity`) VALUES
(2, 'can cooler white', 1, 'can_cooler_white.png', '2019-10-22', 1, 15.99, 5.55, 10000, 'WHITE', '8oz', '', '12oz'),
(3, 'can cooler black', 1, 'can_cooler_black.png', '2019-10-22', 1, 15.99, 5.55, 10000, 'BLACK', '8oz', '', '12oz'),
(4, 'can cooler gray', 1, 'can_cooler_gray.png', '2019-10-22', 1, 15.99, 5.55, 10000, 'SILVER', '8oz', '', '12oz'),
(5, 'tumbler gray', 3, 'tumbler_gray.png', '2019-10-22', 1, 18.99, 5.55, 10000, 'SILVER', '30oz', '', '11oz'),
(6, 'tumbler black', 3, 'tumbler_black.png', '2019-10-22', 1, 18.99, 5.55, 10000, 'BLACK', '30oz', '', '11oz'),
(7, 'tumbler white', 3, 'tumbler_white.png', '2019-10-22', 1, 18.99, 5.55, 10000, 'WHITE', '30oz', '', '11oz'),
(8, 'water bottle black', 4, 'water_bottle_black.png', '2019-10-22', 1, 13.99, 5.55, 10000, 'BLACK', '500ml', '', '11oz'),
(9, 'water bottle white', 4, 'water_bottle_white.png', '2019-10-22', 1, 13.99, 5.55, 10000, 'WHITE', '500ml', '', '11oz'),
(10, 'Sport thermo black', 2, 'sport-Thermo-20oz-black.png', '2019-10-22', 1, 20.99, 5.55, 10000, 'BLACK', '500ml', '', '11oz'),
(11, 'Sport thermo white', 2, 'sport-Thermo-20oz-saintless.png', '2019-10-22', 1, 20.99, 5.55, 10000, 'WHITE', '500ml', '', '11oz'),
(12, 'tumbler gray', 3, 'Tumbler20oz_Steinless.png', '2019-10-22', 1, 16.99, 5.55, 10000, 'SILVER', '20oz', '', '9oz'),
(13, 'tumbler black', 3, 'Tumbler20oz_Black.png', '2019-10-22', 1, 16.99, 5.55, 10000, 'BLACK', '20oz', '', '9oz'),
(14, 'tumbler white', 3, 'Tumbler20oz_Withe.png', '2019-10-22', 1, 16.99, 5.55, 10000, 'WHITE', '20oz', '', '9oz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sell`
--

CREATE TABLE `sell` (
  `id` int(11) NOT NULL,
  `sub_total` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `shippin` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `habilitado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sell`
--

INSERT INTO `sell` (`id`, `sub_total`, `total`, `shippin`, `date`, `id_user`, `habilitado`) VALUES
(6, 21.05, 36.05, 15, '2019-11-05', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `thermos_types`
--

CREATE TABLE `thermos_types` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `thermos_types`
--

INSERT INTO `thermos_types` (`id`, `name`, `create_date`, `enabled`) VALUES
(1, 'can_cooler', '2019-10-22', 1),
(2, 'thermo', '2019-10-22', 1),
(3, 'tumbler', '2019-10-22', 1),
(4, 'water_bottles', '2019-10-22', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_custom`
--

CREATE TABLE `users_custom` (
  `id` int(11) NOT NULL,
  `username` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users_custom`
--

INSERT INTO `users_custom` (`id`, `username`, `password`, `email`, `address`, `city`, `state`, `zipcode`, `access_token`, `create_date`, `enabled`) VALUES
(1, 'Userfily', 'root', '', '', NULL, NULL, NULL, NULL, '2019-10-22', 1),
(2, 'Userfily2', 'root', '', '', NULL, NULL, NULL, NULL, '2019-10-22', 1),
(3, NULL, 'e10adc3949ba59abbe56e057f20f883e', 'filibertocoronado010@gmail.com', '', NULL, NULL, NULL, '08b381a7fe6bf6d75325db2e8ff3266b', '2019-10-24', 1),
(4, NULL, 'a8f5f167f44f4964e6c998dee827110c', 'asdasd@asdasd', '', NULL, NULL, NULL, '6bf5fd160c15e761f26e0df9ac52c462', '2019-10-25', 1),
(5, NULL, '4297f44b13955235245b2497399d7a93', 'asda@asda.com', '', NULL, NULL, NULL, '115277883247949dadacb42d00de4623', '2019-10-25', 1),
(7, NULL, 'c33367701511b4f6020ec61ded352059', 'filibertocoronado01@gmail.com', 'Ajoway 505', 'Tucson', 'Arizona', '85713', '9c121bae8d84470e5d5ac0370dc6f95e', '2019-11-05', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_product` (`id_product`);

--
-- Indices de la tabla `details_sell`
--
ALTER TABLE `details_sell`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sell` (`id_sell`),
  ADD KEY `id_product` (`id_product`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indices de la tabla `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `thermos_types`
--
ALTER TABLE `thermos_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_custom`
--
ALTER TABLE `users_custom`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `details_sell`
--
ALTER TABLE `details_sell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `sell`
--
ALTER TABLE `sell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `thermos_types`
--
ALTER TABLE `thermos_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users_custom`
--
ALTER TABLE `users_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_custom` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Filtros para la tabla `details_sell`
--
ALTER TABLE `details_sell`
  ADD CONSTRAINT `details_sell_ibfk_1` FOREIGN KEY (`id_sell`) REFERENCES `sell` (`id`),
  ADD CONSTRAINT `details_sell_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`type`) REFERENCES `thermos_types` (`id`);

--
-- Filtros para la tabla `sell`
--
ALTER TABLE `sell`
  ADD CONSTRAINT `sell_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_custom` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
