<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $id_product
 * @property int $id_user
 * @property int $status
 * @property string $date
 * @property int $habilitado
 * @property string $message
 * @property string $font
 * @property string $font_size
 * @property string $font_type
 * @property string $image_icon
 * @property string $image_example
 * @property int $cant
 * @property double $price
 * @property string $meesage2
 * @property string $font2
 * @property string $font_size2
 * @property string $image_icon2
 * @property string $image_example2
 *
 * @property UsersCustom $user
 * @property Product $product
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_user', 'status', 'date'], 'required'],
            [['id_product', 'id_user', 'status', 'habilitado', 'cant'], 'integer'],
            [['date'], 'safe'],
            [['message', 'image_icon', 'image_example'], 'string'],
            [['price'], 'number'],
            [['font'], 'string', 'max' => 150],
            [['font_size', 'font_type', 'font2', 'font_size2'], 'string', 'max' => 50],
            [['meesage2', 'image_icon2', 'image_example2'], 'string', 'max' => 100],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => UsersCustom::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_user' => 'Id User',
            'status' => 'Status',
            'date' => 'Date',
            'habilitado' => 'Habilitado',
            'message' => 'Message',
            'font' => 'Font',
            'font_size' => 'Font Size',
            'font_type' => 'Font Type',
            'image_icon' => 'Image Icon',
            'image_example' => 'Image Example',
            'cant' => 'Cant',
            'price' => 'Price',
            'meesage2' => 'Message2',
            'font2' => 'Font2',
            'font_size2' => 'Font Size2',
            'image_icon2' => 'Image Icon2',
            'image_example2' => 'Image Example2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UsersCustom::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
}
