<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cupon".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property string $date
 * @property string $percentaje
 */
class Cupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cupon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'enabled', 'date', 'percentaje'], 'required'],
            [['enabled'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['percentaje'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'date' => 'Date',
            'percentaje' => 'Percentaje',
        ];
    }
}
