<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "details_sell".
 *
 * @property int $id
 * @property int $id_product
 * @property int $id_sell
 * @property string $date
 * @property double $price
 * @property int $habilitado
 *
 * @property Sell $sell
 * @property Product $product
 */
class DetailsSell extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'details_sell';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_sell', 'date', 'price'], 'required'],
            [['id_product', 'id_sell', 'habilitado'], 'integer'],
            [['date'], 'safe'],
            [['price'], 'number'],
            [['id_sell'], 'exist', 'skipOnError' => true, 'targetClass' => Sell::className(), 'targetAttribute' => ['id_sell' => 'id']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_sell' => 'Id Sell',
            'date' => 'Date',
            'price' => 'Price',
            'habilitado' => 'Habilitado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSell()
    {
        return $this->hasOne(Sell::className(), ['id' => 'id_sell']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }

  
    public function getProductDto()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product'])->select("id,name");
    }
}
