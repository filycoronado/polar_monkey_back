<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $image
 * @property string $create_date
 * @property int $enabled
 * @property double $price_sell
 * @property double $price_buy
 * @property int $stock
 * @property string $color
 * @property string $weight
 * @property string $description
 * @property string $capacity
 *
 * @property Cart[] $carts
 * @property DetailsSell[] $detailsSells
 * @property ThermosTypes $type0
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'enabled', 'stock'], 'integer'],
            [['image'], 'string'],
            [['create_date'], 'safe'],
            [['price_sell', 'price_buy'], 'number'],
            [['name'], 'string', 'max' => 80],
            [['color'], 'string', 'max' => 40],
            [['weight', 'capacity'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 100],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => ThermosTypes::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'image' => 'Image',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
            'price_sell' => 'Price Sell',
            'price_buy' => 'Price Buy',
            'stock' => 'Stock',
            'color' => 'Color',
            'weight' => 'Weight',
            'description' => 'Description',
            'capacity' => 'Capacity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailsSells()
    {
        return $this->hasMany(DetailsSell::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(ThermosTypes::className(), ['id' => 'type']);
    }
}
