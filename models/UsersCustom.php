<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_custom".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $access_token
 * @property string $create_date
 * @property int $enabled
 *
 * @property Cart[] $carts
 * @property Sell[] $sells
 */
class UsersCustom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_custom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'access_token'], 'string'],
            [['email'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['username', 'email'], 'string', 'max' => 150],
            [['address'], 'string', 'max' => 100],
            [['city'], 'string', 'max' => 25],
            [['state'], 'string', 'max' => 50],
            [['zipcode'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'zipcode' => 'Zipcode',
            'access_token' => 'Access Token',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSells()
    {
        return $this->hasMany(Sell::className(), ['id_user' => 'id']);
    }
}
